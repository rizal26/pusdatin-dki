<style>
    .overlay-bg {
        height: 480px;
        overflow: hidden;
        background-size: cover;
        border-radius: 0.25rem;
        filter: blur(3px);
        -webkit-filter: blur(3px);
    }
    .card-1 {
        background-image: url("assets/images/1.png");
    }
    .card-2 {
        background-image: url("assets/images/2.png");
    }
    .card-3 {
        background-image: url("assets/images/3.png");
    }
    .card-4 {
        background-image: url("assets/images/4.jpg");
    }
    .card-5 {
        background-image: url("assets/images/5.jpg");
    }
    .card-6 {
        background-image: url("assets/images/6.jpg");
    }
    .card-body {
        padding: 0.5rem;
    }
    .card-body > p {
        text-align: center;
    }
    table > tbody {
        font-size: 12px;
    }
</style>

<div class="row match-height">
    <div class="col-xl-3 col-md-6 col-sm-12">
        <div class="card overlay-img-card text-white">
            <div class="overlay-bg card-1"></div>
            <div class="card-img-overlay overlay-black">
                <p class="text-white text-center font-large-4 mt-2 mb-2">
                    <i class="feather icon-users"></i>
                </p>
                <div class="card-content">
                    <div class="card-body">
                        <p>Jumlah Data Terpadu Kesejahteraan Sosial (DTKS) : 371.642 ID DTKS</p>
                        <p>Jumlah Anggota Rumah Tangga (ART) : 1.447.095 NIK</p>
                        <p>Periode Januari 2020</p>
                    </div>
                    <div
                        style="position: absolute; bottom: 0; left: 50%; transform: translate(-50%, -50%);">
                        <button
                            type="button"
                            class="btn btn-primary round mb-1 waves-effect waves-light"
                            data-toggle="tooltip"
                            data-placement="top"
                            title=""
                            data-original-title="Untuk melihat lebih lengkap silahkan klik tombol ini">Show</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-xl-3 col-md-6 col-sm-12">
        <div class="card overlay-img-card text-white">
            <div class="overlay-bg card-2"></div>
            <div class="card-img-overlay overlay-black">
                <p class="text-white text-center font-large-4 mt-2 mb-2">
                    <i class="feather icon-globe"></i>
                </p>
                <div class="card-content">
                    <div class="card-body">
                        <p>0 dari total 371.642 Penyebaran DTKS</p>
                        <p>Periode Januari 2020</p>
                    </div>
                    <div
                        style="position: absolute; bottom: 0; left: 50%; transform: translate(-50%, -50%);">
                        <button
                            type="button"
                            class="btn btn-primary round mb-1 waves-effect waves-light"
                            data-toggle="tooltip"
                            data-placement="top"
                            title=""
                            data-original-title="Untuk melihat lebih lengkap silahkan klik tombol ini">Show</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-xl-3 col-md-6 col-sm-12">
        <div class="card overlay-img-card text-white">
            <div class="overlay-bg card-3"></div>
            <div class="card-img-overlay overlay-black">
                <p class="text-white text-center font-large-4 mt-2 mb-2">
                    <i class="feather icon-file-text"></i>
                </p>
                <div class="card-content">
                    <div class="card-body">
                        <p>371.642 DTKS dari total 371.642 DTKS yang sudah diperingkat untuk
                            direkomendasikan</p>
                        <p>Periode Januari 2020</p>
                    </div>
                    <div
                        style="position: absolute; bottom: 0; left: 50%; transform: translate(-50%, -50%);">
                        <button
                            type="button"
                            class="btn btn-primary round mb-1 waves-effect waves-light"
                            data-toggle="tooltip"
                            data-placement="top"
                            title=""
                            data-original-title="Untuk melihat lebih lengkap silahkan klik tombol ini">Show</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-xl-3 col-md-6 col-sm-12">
        <div class="card overlay-img-card text-white">
            <div class="overlay-bg card-4"></div>
            <div class="card-img-overlay overlay-black">
                <p class="text-white text-center font-large-4 mt-2 mb-2">
                    <i class="feather icon-calendar"></i>
                </p>
                <div class="card-content">
                    <div class="card-body">
                        <p>Rekap Data DTKS Seluruh Periode</p>
                    </div>
                    <div
                        style="position: absolute; bottom: 0; left: 50%; transform: translate(-50%, -50%);">
                        <button
                            type="button"
                            class="btn btn-primary round mb-1 waves-effect waves-light"
                            data-toggle="tooltip"
                            data-placement="top"
                            title=""
                            data-original-title="Untuk melihat data pada periode sebelumnya silahkan klik tombol ini">Rekap</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row match-height">
    <div class="col-xl-6 col-sm-12">
        <div class="card overlay-img-card text-white">
            <div class="overlay-bg card-5"></div>
            <div class="card-img-overlay overlay-black">
                <p class="text-white text-center font-large-4 mt-2 mb-2">
                    <i class="feather icon-bar-chart-2"></i>
                </p>
                <div class="card-content">
                    <div class="card-body">
                        <p>Rekap Data DTKS Per Periode</p>
                        <canvas id="dtks" width="300" height="150"></canvas>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-xl-6 col-sm-12">
        <div class="card overlay-img-card text-white">
            <div class="overlay-bg card-6"></div>
            <div class="card-img-overlay overlay-black">
                <p class="text-white text-center font-large-4 mt-2 mb-2">
                    <i class="feather icon-bar-chart-2"></i>
                </p>
                <div class="card-content">
                    <div class="card-body">
                        <p>Resume Periode Januari 2020</p>
                        <canvas id="resume" width="300" height="150"></canvas>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-header">
                <div class="col-md-12 text-center">
                    <p class="text-bold-700">Total keseluruhan sangat miskin 1.150.231 jiwa, miskin 123.626 jiwa, hampir miskin 66.137 jiwa, rentan miskin 63.234 jiwa, dan tidak miskin 43.867 jiwa</p>
                </div>
            </div>
            <div class="card-content">
                <div class="card-body">
                    <table class="table w-100" id="table-dashboard">
                        <thead>
                            <th>Kode Wilayah</th>
                            <th>Kabupaten / Kota</th>
                            <th>Kecamatan</th>
                            <th>Kelurahan</th>
                            <th>Sangat Miskin</th>
                            <th>Miskin</th>
                            <th>Hampir Miskin</th>
                            <th>Rentant Miskin</th>
                            <th>Tidak Miskin</th>
                        </thead>
                        <tbody>
                            <tr>
                                <td>3101010001</td>
                                <td>KEPULAUAN SERIBU</td>
                                <td>KEPULAUAN SERIBU SELATAN</td>
                                <td>PULAU TIDUNG</td>
                                <td>1473</td>
                                <td>53</td>
                                <td>7</td>
                                <td>9</td>
                                <td>15</td>
                            </tr>
                            <tr>
                                <td>3171010001</td>
                                <td>JAKARTA SELATAN</td>
                                <td>JAGAKARSA</td>
                                <td>CIPEDAK</td>
                                <td>4395</td>
                                <td>181</td>
                                <td>69</td>
                                <td>80</td>
                                <td>21</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="app-assets/vendors/js/charts/chart.js"></script>

<script>
    $(function () {
        $('#table-dashboard').DataTable({
            scrollX: true,
            responsive: true,
            bLengthChange: false,
            bFilter: false,
            bInfo: false,
            bSort: false
        });
    });

    var ctx = document.getElementById('dtks').getContext('2d');
    var myChart = new Chart(ctx, {
        type: 'bar',
        responsive: true,
        data: {
            labels: ['Januari 2021', 'Februari 2021', 'Maret 2021', 'April 2021'],
            datasets: [{
                label: 'Total DTKS',
                data: [450, 200, 340, 115],
                color: '#fff',
                backgroundColor: [
                    'rgba(255, 99, 132, 0.3)',
                    'rgba(54, 162, 235, 0.3)',
                    'rgba(255, 206, 86, 0.3)',
                    'rgba(75, 192, 192, 0.3)'
                ],
                borderColor: [
                    'rgba(255, 99, 132, 1)',
                    'rgba(54, 162, 235, 1)',
                    'rgba(255, 206, 86, 1)',
                    'rgba(75, 192, 192, 1)'
                ],
                borderWidth: 1,
            }]
        },
        options: {
            legend: {
                display: false
            },
            scales: {
                yAxes: [{
                    ticks: {
                        fontColor: "white",
                        beginAtZero: true,
                        min: 0,
                        max: 500,
                        stepSize: 100 
                    }
                }],
                xAxes: [{
                    ticks: {
                        fontColor: "white"
                    }
                }]
            }
        }
    });

    var ctx2 = document.getElementById('resume').getContext('2d');
    var myChart2 = new Chart(ctx2, {
        type: 'bar',
        responsive: true,
        data: {
            labels: ['KS', 'JS', 'JT', 'JP', 'JB', 'JU'],
            datasets: [{
                label: 'Total DTKS',
                data: [7542, 314808, 352624, 190500, 240000, 330000],
                color: '#fff',
                backgroundColor: [
                    'rgba(255, 99, 132, 0.3)',
                    'rgba(54, 162, 235, 0.3)',
                    'rgba(255, 206, 86, 0.3)',
                    'rgba(75, 192, 192, 0.3)',
                    'rgba(153, 102, 255, 0.3)',
                    'rgba(255, 159, 64, 0.3)'
                ],
                borderColor: [
                    'rgba(255, 99, 132, 1)',
                    'rgba(54, 162, 235, 1)',
                    'rgba(255, 206, 86, 1)',
                    'rgba(75, 192, 192, 1)',
                    'rgba(153, 102, 255, 1)',
                    'rgba(255, 159, 64, 1)'
                ],
                borderWidth: 1,
            }]
        },
        options: {
            legend: {
                display: false
            },
            scales: {
                yAxes: [{
                    ticks: {
                        fontColor: "white",
                        beginAtZero: true,
                        min: 0,
                        max: 400000,
                        stepSize: 100000 
                    }
                }],
                xAxes: [{
                    ticks: {
                        fontColor: "white"
                    }
                }]
            }
        }
    });
</script>