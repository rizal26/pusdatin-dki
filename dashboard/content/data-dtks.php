<style>
    #card-info tr td {
        padding: 3px;
        vertical-align: top;
        font-weight: 500;
    }
    i {
        padding-right: 20px;
    }
    #table-data-dtks > tbody {
        font-size: 12px;
    }
    th { 
        text-align: center;
        vertical-align: middle !important; 
    }

    #bread-actions {
        text-align: center;
    }
    
</style>

<div class="row">
    <div class="col-lg-7 col-sm-12">
        <div class="card" style="background-color: #e8e8e8">
            <div
                class="card-header d-flex flex-column align-items-start"
                style="padding: 10px">
                <div class="row w-100">
                    <div class="col-12">
                        <div class="table-responsive mt-0">
                            <table id="card-info" class="w-100 table-hover">
                                <tr>
                                    <td>
                                        <i class="feather icon-home"></i>Jumlah Data Terpadu Kesejahteraan Sosial (DTKS)</td>
                                    <td>:</td>
                                    <td>371.642 ID DTKS</td>
                                </tr>
                                <tr>
                                    <td>
                                        <i class="feather icon-users"></i>Jumlah Anggota Rumah Tangga (ART)</td>
                                    <td>:</td>
                                    <td>1.447.095 NIK</td>
                                </tr>
                                <tr>
                                    <td colspan="3">
                                        <i class="feather icon-calendar"></i>Periode Januari 2020</td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-content">
                <div class="card-body">
                    <div class="form-body">
                        <div class="row">
                            <div class="col-12">
                                <div class="form-group">
                                    <label for=""></label>
                                    <div class="position-relative">
                                        <fieldset class="form-label-group form-group position-relative">
                                            <input
                                                type="text"
                                                class="form-control"
                                                id="iconLabelRight"
                                                placeholder="Search"/>
                                            <div class="form-control-position">
                                                <a href="javascript:void(0)">
                                                    <i class="feather icon-search"></i>
                                                </a>
                                            </div>
                                            <label for="iconLabelRight">Search</label>
                                        </fieldset>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <table id="table-data-dtks" class="table table-hover table-bordered complex-headers w-100">
                                    <thead>
                                        <tr>
                                            <th>No.</th>
                                            <th>IDBDT</th>
                                            <th>Alamat</th>
                                            <th>Nama SLS</th>
                                            <th>Nama KRT</th>
                                            <th>Jumlah ART</th>
                                            <th>Jumlah Keluarga</th>
                                            <th>Flag Location</th>
                                            <th>Rupiah</th>
                                            <th>Percentile</th>
                                            <th>Latitude</th>
                                            <th>Longitude</th>
                                            <th>Actions</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>
                                                1.
                                            </td>
                                            <td>3172070007000531</td>
                                            <td>JLKP KAPITAN</td>
                                            <td> </td>
                                            <td>SLAMET</td>
                                            <td>11</td>
                                            <td>1</td>
                                            <td>0</td>
                                            <td>Rp. 11,085.77</td>
                                            <td>0</td>
                                            <td> </td>
                                            <td> </td>
                                            <td class="no-sort no-click" id="bread-actions">
                                                <a
                                                    href="javascript:void(0)"
                                                    title="View"
                                                    class="btn btn-sm btn-warning">
                                                    <span class="hidden-xs hidden-sm">View</span>
                                                </a>

                                                <a
                                                    href="javascript:void(0)"
                                                    title="API"
                                                    class="btn btn-sm btn-success"
                                                    style="margin-top: 5px">
                                                    
                                                    <span class="hidden-xs hidden-sm">ART</span>
                                                </a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                2.
                                            </td>
                                            <td>3174070001001405</td>
                                            <td>JL TIMBUL JAYA RT 005 RW 05</td>
                                            <td> </td>
                                            <td>PARDI NOAR</td>
                                            <td>14</td>
                                            <td>1</td>
                                            <td>0</td>
                                            <td>Rp. 11,402.40</td>
                                            <td>4</td>
                                            <td> </td>
                                            <td> </td>
                                            <td class="no-sort no-click" id="bread-actions">
                                                <a
                                                    href="javascript:void(0)"
                                                    title="View"
                                                    class="btn btn-sm btn-warning">
                                                    <span class="hidden-xs hidden-sm">View</span>
                                                </a>

                                                <a
                                                    href="javascript:void(0)"
                                                    title="API"
                                                    class="btn btn-sm btn-success"
                                                    style="margin-top: 5px">
                                                    
                                                    <span class="hidden-xs hidden-sm">ART</span>
                                                </a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                3.
                                            </td>
                                            <td>3172080002001158</td>
                                            <td>KPJEMBATAN</td>
                                            <td> </td>
                                            <td>MATZEN</td>
                                            <td>11</td>
                                            <td>1</td>
                                            <td>0</td>
                                            <td>Rp. 11,411.02</td>
                                            <td>0</td>
                                            <td> </td>
                                            <td> </td>
                                            <td class="no-sort no-click" id="bread-actions">
                                                <a
                                                    href="javascript:void(0)"
                                                    title="View"
                                                    class="btn btn-sm btn-warning">
                                                    <span class="hidden-xs hidden-sm">View</span>
                                                </a>

                                                <a
                                                    href="javascript:void(0)"
                                                    title="API"
                                                    class="btn btn-sm btn-success"
                                                    style="margin-top: 5px">
                                                    
                                                    <span class="hidden-xs hidden-sm">ART</span>
                                                </a>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        $(function () {
            $('#table-data-dtks').DataTable({
                scrollX: true,
                responsive: true,
                bLengthChange: false,
                bFilter: false,
                bInfo: false,
                bSort: false
            });
        });
    </script>