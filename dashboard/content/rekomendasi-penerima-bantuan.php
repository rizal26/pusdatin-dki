<style>
    #card-info tr td {
        padding: 3px;
        vertical-align: top;
        font-weight: 500;
    }
    i {
        padding-right: 20px;
    }
    #table-pemeringkatan > tbody {
        font-size: 12px;
    }
</style>

<div class="row">
    <div class="col-lg-8 col-sm-12">
        <div class="card" style="background-color: #e8e8e8">
            <div
                class="card-header d-flex flex-column align-items-start"
                style="padding: 10px">
                <div class="row w-100">
                    <div class="col-12">
                        <div class="table-responsive mt-0">
                            <table id="card-info" class="w-100 table-hover">
                                <tr>
                                    <td>
                                        <i class="feather icon-file"></i>(371.642 DTKS dari total 371.642 DTKS) yang sudah diperingkat untuk direkomendasikan</td>
                                </tr>
                                <tr>
                                    <td>
                                        <i class="feather icon-calendar"></i>Periode Januari 2020</td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-content">
                <div class="card-body">
                    <div class="form-body">
                        <div class="row">
                            <div class="col-4">
                                <div class="form-group">
                                    <label for="">Kabupaten / Kota</label>
                                    <div class="position-relative">
                                        <select name="" id="" class="form-control">
                                            <option selected="selected" disabled="disabled">Pilih Kabupaten / Kota</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-4">
                                <div class="form-group">
                                    <label for="">Kecamatan</label>
                                    <div class="position-relative">
                                        <select name="" id="" class="form-control">
                                            <option selected="selected" disabled="disabled">Pilih Kecamatan</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-4">
                                <div class="form-group">
                                    <label for="">Kelurahan</label>
                                    <div class="position-relative">
                                        <select name="" id="" class="form-control">
                                            <option selected="selected" disabled="disabled">Pilih Kelurahan</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-4">
                                <div class="form-group">
                                    <label for="">Program Bantuan</label>
                                    <div class="position-relative">
                                        <select name="" id="" class="form-control">
                                            <option selected="selected" disabled="disabled">Pilih Program Bantuan</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-4">
                                <div class="form-group">
                                    <label for="">Kategori</label>
                                    <div class="position-relative">
                                        <select name="" id="" class="form-control">
                                            <option selected="selected" disabled="disabled">Pilih Kategori</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-2">
                                <div class="form-group">
                                    <label for="">Umur (From - To)</label>
                                    <div class="position-relative">
                                        <select name="" id="" class="form-control">
                                            <option selected="selected" disabled="disabled">From</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-2">
                                <div class="form-group">
                                    <label for=""></label>
                                    <div class="position-relative">
                                        <select name="" id="" class="form-control">
                                            <option selected="selected" disabled="disabled">To</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <div class="form-group pull-right">
                                    <button type="button" class="btn btn-primary waves-effect waves-light">Search</button>
                                    <button type="button" class="btn btn-success waves-effect waves-light">Download</button>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6 col-sm-12">
                                <canvas id="chart-bar" width="300" height="150"></canvas>
                            </div>
                            <div class="col-lg-6 col-sm-12">
                                <canvas id="chart-pie" width="300" height="135"></canvas>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script src="app-assets/vendors/js/charts/chart.js"></script>
    <script src="app-assets/vendors/js/charts/chartjs-plugin-datalabels.js"></script>
    

    <script>
        var ctx = document
            .getElementById('chart-bar')
            .getContext('2d');
        var myChart = new Chart(ctx, {
            type: 'bar',
            responsive: true,
            data: {
                labels: ['Ciledug'],
                datasets: [
                    {
                        label: 'Tidak Miskin',
                        data: [50],
                        color: '#fff',
                        backgroundColor: ['rgba(255, 99, 132, 0.3)'],
                        borderColor: ['rgba(255, 99, 132, 1)'],
                        borderWidth: 1
                    }
                ]
            },
            options: {
                legend: {
                    display: true
                },
                scales: {
                    yAxes: [
                        {
                            ticks: {
                                beginAtZero: true,
                                min: 0,
                                max: 55,
                                stepSize: 15
                            }
                        }
                    ]
                },
                plugins: {
                    datalabels: {
                        display: false
                    }
                }
            }
        });

        var ctx_2 = document
            .getElementById('chart-pie')
            .getContext('2d');
        var myChart_2 = new Chart(ctx_2, {
            type: 'pie',
            responsive: true,
            data: {
                labels: ['Ciledug'],
                datasets: [
                    {
                        data: [50],
                        backgroundColor: ['rgba(255, 206, 86, 0.3)'],
                        borderColor: ['rgba(255, 206, 86, 1)'],
                        borderWidth: 1,
                        hoverOffset: 4
                    }
                ]
            },
            options: {
                plugins: {
                    datalabels: {
                        formatter: (value) => {
                            return value+'\n'+ 100 + '%';
                        }
                    }
                }
            }
        });
    </script>