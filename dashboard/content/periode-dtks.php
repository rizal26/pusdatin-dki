<style>
    #card-info tr td {
        padding: 3px;
        vertical-align: top;
        font-weight: 500;
    }
    i {
        padding-right: 20px;
    }
    #table-periode-dtks > tbody tr td:first-child, 
    #table-periode-dtks > tbody tr td:last-child {
        text-align: center;
    }

    th { 
        text-align: center;
        vertical-align: middle !important; 
    }
    #bread-actions {
        text-align: center;
    }
    
</style>

<div class="row">
    <div class="col-lg-7 col-sm-12">
        <div class="card" style="background-color: #e8e8e8">
            <div
                class="card-header d-flex flex-column align-items-start"
                style="padding: 10px">
                <div class="row w-100">
                    <div class="col-12">
                        <div class="table-responsive mt-0">
                            <table id="card-info" class="w-100 table-hover">
                                <tr>
                                    <td>
                                        <i class="feather icon-calendar"></i>Resume Periode Januari 2020</td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-5 col-sm-12">
        <div class="pull-right">
            <button type="button" class="btn btn-primary">
                <span class="feather icon-plus"></span> Add New
            </button>
            <button type="button" class="btn btn-danger">
                <span class="feather icon-trash"></span> Bulk Delete
            </button>
        </div>
    </div>
</div>
<div class="row" style="margin-top: 20px">
    <div class="col-12">
        <div class="card">
            <div class="card-content">
                <div class="card-body">
                    <div class="form-body">
                        <div class="row">
                            <div class="col-12">
                                <table id="table-periode-dtks" class="table table-hover table-bordered complex-headers w-100">
                                    <thead>
                                        <tr>
                                            <th><input type="checkbox" id="selectall" class="bulk" value="all" style="transform:scale(1.4)" /></th>
                                            <th>No.</th>
                                            <th>Bulan</th>
                                            <th>Tahun</th>
                                            <th>Status</th>
                                            <th style="width:30%;">Actions</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td><input type="checkbox" class="bulk" value="" style="transform:scale(1.4)" /></td>
                                            <td>5</td>
                                            <td>Januari</td>
                                            <td>2020</td>
                                            <td>Aktif</td>
                                            <td>
                                                <button type="button" class="btn btn-sm btn-warning">
                                                    <span class="feather icon-eye"></span> View
                                                </button>
                                                <button type="button" class="btn btn-sm btn-success">
                                                    <span class="feather icon-edit"></span> Edit
                                                </button>
                                                <button type="button" class="btn btn-sm btn-danger">
                                                    <span class="feather icon-trash"></span> Delete
                                                </button>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td><input type="checkbox" class="bulk" value="" style="transform:scale(1.4)" /></td>
                                            <td>3</td>
                                            <td>Oktober</td>
                                            <td>2019</td>
                                            <td>Non Aktif</td>
                                            <td>
                                                <button type="button" class="btn btn-sm btn-warning">
                                                    <span class="feather icon-eye"></span> View
                                                </button>
                                                <button type="button" class="btn btn-sm btn-success">
                                                    <span class="feather icon-edit"></span> Edit
                                                </button>
                                                <button type="button" class="btn btn-sm btn-danger">
                                                    <span class="feather icon-trash"></span> Delete
                                                </button>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td><input type="checkbox" class="bulk" value="" style="transform:scale(1.4)" /></td>
                                            <td>2</td>
                                            <td>Juli</td>
                                            <td>2019</td>
                                            <td>Non Aktif</td>
                                            <td>
                                                <button type="button" class="btn btn-sm btn-warning">
                                                    <span class="feather icon-eye"></span> View
                                                </button>
                                                <button type="button" class="btn btn-sm btn-success">
                                                    <span class="feather icon-edit"></span> Edit
                                                </button>
                                                <button type="button" class="btn btn-sm btn-danger">
                                                    <span class="feather icon-trash"></span> Delete
                                                </button>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td><input type="checkbox" class="bulk" value="" style="transform:scale(1.4)" /></td>
                                            <td>1</td>
                                            <td>Januari</td>
                                            <td>2019</td>
                                            <td>Non Aktif</td>
                                            <td>
                                                <button type="button" class="btn btn-sm btn-warning">
                                                    <span class="feather icon-eye"></span> View
                                                </button>
                                                <button type="button" class="btn btn-sm btn-success">
                                                    <span class="feather icon-edit"></span> Edit
                                                </button>
                                                <button type="button" class="btn btn-sm btn-danger">
                                                    <span class="feather icon-trash"></span> Delete
                                                </button>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        $(function () {
            $('#selectall').click(function () { 
                $('.bulk:checkbox').not(this).prop('checked', this.checked);
            });

            $('#table-periode-dtks').DataTable({
                scrollX: true,
                responsive: true,
                // bLengthChange: false,
                // bFilter: false,
                // bInfo: false,
                bSort: false
            });
        });
    </script>