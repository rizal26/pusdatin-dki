<style>
    #card-info tr td {
        padding: 3px;
        vertical-align: top;
        font-weight: 500;

    }
    i {
        padding-right: 20px;
    }
    #table-bantuan-berbasis-nik > tbody {
        /* font-size: 12px; */
        text-align: center;
    }
    th { 
        text-align: center;
    }
</style>

<div class="row">
    <div class="col-lg-6 col-sm-12">
        <div class="card" style="background-color: #e8e8e8">
            <div
                class="card-header d-flex flex-column align-items-start"
                style="padding: 10px">
                <div class="row w-100">
                    <div class="col-12">
                        <div class="table-responsive mt-0">
                            <table id="card-info" class="w-100 table-hover">
                                <tr>
                                    <td>
                                        <i class="feather icon-package"></i>Penerima Bantuan Tahun 2020</td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-content">
                <div class="card-body">
                    <div class="form-body">
                        <div class="row">
                            <div class="col-4">
                                <div class="form-group">
                                    <label for="">Kabupaten / Kota</label>
                                    <div class="position-relative">
                                        <select name="" id="" class="form-control">
                                            <option selected="selected" disabled="disabled">Pilih Kabupaten / Kota</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-4">
                                <div class="form-group">
                                    <label for="">Kecamatan</label>
                                    <div class="position-relative">
                                        <select name="" id="" class="form-control">
                                            <option selected="selected" disabled="disabled">Pilih Kecamatan</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-4">
                                <div class="form-group">
                                    <label for="">Kelurahan</label>
                                    <div class="position-relative">
                                        <select name="" id="" class="form-control">
                                            <option selected="selected" disabled="disabled">Pilih Kelurahan</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-6">
                                <div class="form-group">
                                    <label for="">Program Bantuan</label>
                                    <div class="position-relative">
                                        <select name="" id="" class="form-control">
                                            <option selected="selected" disabled="disabled">Pilih Program Bantuan</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="form-group">
                                    <label for="">NIK</label>
                                    <div class="position-relative">
                                        <input type="text" class="form-control" placeholder="Search by NIK"/></div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                Total data yang ditampilkan 1.447.095 jiwa.
                                <div class="form-group pull-right">
                                    <button type="button" class="btn btn-primary waves-effect waves-light">Search</button>
                                    <button type="button" class="btn btn-success waves-effect waves-light">Download</button>
                                    <button type="button" class="btn btn-warning waves-effect waves-light">Non DTKS</button>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <table class="complex-headers table-bordered table-striped w-100" id="table-bantuan-berbasis-nik">
                                    <thead>
                                        <tr>
                                            <th rowspan="2">IDDTKS</th>
                                            <th rowspan="2">No KK</th>
                                            <th rowspan="2">NIK</th>
                                            <th colspan="4">Program Bantuan APBN</th>
                                            <th colspan="5">Program bantuan APBD</th>
                                            <th rowspan="2" style="border-left: 1px solid #dae1e7; !important">Total</th>
                                        </tr>
                                        <tr>
                                            <th>KKS</th>
                                            <th>PBI</th>
                                            <th>PKH</th>
                                            <th>B.Pangan<br>Sosial</th>
                                            <th>PKD<br>Anak</th>
                                            <th>PKD<br>Disabilitas</th>
                                            <th>KLJ</th>
                                            <th>KJP<br>Plus</th>
                                            <th>KJMU</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>3101010001000001</td>
                                            <td>3101020603090568</td>
                                            <td>3101025312830001</td>
                                            <td>✓</td>
                                            <td>✓</td>
                                            <td>✓</td>
                                            <td>✓</td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td>4</td>
                                        </tr>
                                        <tr>
                                            <td>3101010001000001</td>
                                            <td>3101020603090568</td>
                                            <td>3101022704740001</td>
                                            <td>✓</td>
                                            <td>✓</td>
                                            <td>✓</td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td>3</td>
                                        </tr>
                                        <tr>
                                            <td>3101010001000003</td>
                                            <td>3101020603091458</td>
                                            <td>3101021808000001</td>
                                            <td>✓</td>
                                            <td>✓</td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td>2</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        $(function () {
            $('#table-bantuan-berbasis-nik').DataTable({
                scrollX: true,
                responsive: true,
                bLengthChange: false,
                bFilter: false,
                bInfo: false,
                bSort: false
            });
        });
    </script>