<style>
    #card-info tr td {
        padding: 3px;
        vertical-align: top;
        font-weight: 500;
    }
    i {
        padding-right: 20px;
    }
    #table-pemeringkatan > tbody {
        font-size: 12px;
    }
</style>

<div class="row">
    <div class="col-lg-7 col-sm-12">
        <div class="card" style="background-color: #e8e8e8">
            <div
                class="card-header d-flex flex-column align-items-start"
                style="padding: 10px">
                <div class="row w-100">
                    <div class="col-12">
                        <div class="table-responsive mt-0">
                            <table id="card-info" class="w-100 table-hover">
                                <tr>
                                    <td>
                                        <i class="feather icon-home"></i>Jumlah Data Terpadu Kesejahteraan Sosial (DTKS)</td>
                                    <td>:</td>
                                    <td>371.642 ID DTKS</td>
                                </tr>
                                <tr>
                                    <td>
                                        <i class="feather icon-file"></i>Jumlah Kepala Keluarga</td>
                                    <td>:</td>
                                    <td>368.057</td>
                                </tr>
                                <tr>
                                    <td>
                                        <i class="feather icon-users"></i>Jumlah Anggota Rumah Tangga (ART)</td>
                                    <td>:</td>
                                    <td>1.447.095 NIK</td>
                                </tr>
                                <tr>
                                    <td colspan="3">
                                        <i class="feather icon-calendar"></i>Periode Januari 2020</td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-content">
                <div class="card-body">
                    <div class="form-body">
                        <div class="row">
                            <div class="col-6">
                                <div class="form-group">
                                    <label for="">Kabupaten / Kota</label>
                                    <div class="position-relative">
                                        <select name="" id="" class="form-control">
                                            <option selected="selected" disabled="disabled">Pilih Kabupaten / Kota</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="form-group">
                                    <label for="">Kecamatan</label>
                                    <div class="position-relative">
                                        <select name="" id="" class="form-control">
                                            <option selected="selected" disabled="disabled">Pilih Kecamatan</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-6">
                                <div class="form-group">
                                    <label for="">Kelurahan</label>
                                    <div class="position-relative">
                                        <select name="" id="" class="form-control">
                                            <option selected="selected" disabled="disabled">Pilih Kelurahan</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="form-group">
                                    <label for="">Kategori</label>
                                    <div class="position-relative">
                                        <select name="" id="" class="form-control">
                                            <option selected="selected" disabled="disabled">Pilih Kategori</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <div class="form-group pull-right">
                                    <button type="button" class="btn btn-primary waves-effect waves-light">Search</button>
                                    <button type="button" class="btn btn-success waves-effect waves-light">Download</button>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <table class="table w-100" id="table-pemeringkatan">
                                    <thead>
                                        <th>Kategori</th>
                                        <th>IDBDT</th>
                                        <th>NIK</th>
                                        <th>No KK</th>
                                        <th>Nama Kepala Keluarga</th>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>Tidak Miskin</td>
                                            <td>3101010001000001</td>
                                            <td>3101022704740001</td>
                                            <td>3101020603090568</td>
                                            <td>SUHERI</td>
                                        </tr>
                                        <tr>
                                            <td>Tidak Miskin</td>
                                            <td>3101010001000004</td>
                                            <td>3101022803960001</td>
                                            <td>3101020603090676</td>
                                            <td>PAJAR PRIYANTO</td>
                                        </tr>
                                        <tr>
                                            <td>Tidak Miskin</td>
                                            <td>3101010001000006</td>
                                            <td>3101020704710001</td>
                                            <td>3101020603092078</td>
                                            <td>SUKARSA</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

<script>
     $(function () {
        $('#table-pemeringkatan').DataTable({
            scrollX: true,
            responsive: true,
            bLengthChange: false,
            bFilter: false,
            bInfo: false,
            bSort: false
        });
    });
</script>