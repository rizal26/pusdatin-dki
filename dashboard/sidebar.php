<div class="navbar-header">
    <ul class="nav navbar-nav flex-row">
        <li class="nav-item mr-auto">
            <a class="navbar-brand" href="dashboard">
                <img src="assets/images/pusdatin-dki-logo.png" width="35" alt="">
                    <h2 class="brand-text mb-0"></h2>
                </a>
            </li>
            <li class="nav-item nav-toggle">
                <a class="nav-link modern-nav-toggle pr-0" data-toggle="collapse">
                    <i class="feather icon-x d-block d-xl-none font-medium-4 primary toggle-icon"></i>
                    <i
                        class="toggle-icon feather icon-disc font-medium-4 d-none d-xl-block collapse-toggle-icon primary"
                        data-ticon="icon-disc"></i>
                </a>
            </li>
        </ul>
    </div>
    <div class="shadow-bottom"></div>
    <div class="main-menu-content">
        <ul
            class="navigation navigation-main"
            id="main-menu-navigation"
            data-menu="menu-navigation">
            <li class=" nav-item" id="dashboard">
                <a href="dashboard">
                    <i class="feather icon-home"></i>
                    <span class="menu-title">Dashboard</span>
                </a>
            </li>
            <li class=" nav-item" id="proxy">
                <a href="#">
                    <i class="feather icon-folder"></i>
                    <span class="menu-title">Proxy Mean Testing</span>
                </a>
                <ul class="menu-content">
                    <li id="pemeringkatan">
                        <a
                            href="dashboard?page=pemeringkatan"
                            data-toggle="tooltip"
                            data-placement="right"
                            title=""
                            data-original-title="Pemeringkatan">
                            <i class="feather icon-circle"></i>
                            <span class="menu-item">Pemeringkatan</span>
                        </a>
                    </li>
                    <li id="penyebaran-tingkat-kemiskinan">
                        <a
                            href="dashboard?page=penyebaran-tingkat-kemiskinan"
                            data-toggle="tooltip"
                            data-placement="right"
                            title=""
                            data-original-title="Penyebaran Tingkat Kemiskinan">
                            <i class="feather icon-circle"></i>
                            <span class="menu-item">Penyebaran Tingkat Kemiskinan</span>
                        </a>
                    </li>
                    <li id="rekomendasi-penerima-bantuan">
                        <a
                            href="dashboard?page=rekomendasi-penerima-bantuan"
                            data-toggle="tooltip"
                            data-placement="right"
                            title=""
                            data-original-title="Rekomendasi Penerima Bantuan">
                            <i class="feather icon-circle"></i>
                            <span class="menu-item">Rekomendasi Penerima Bantuan</span>
                        </a>
                    </li>
                    <li id="bantuan-berbasis-wilayah">
                        <a
                            href="dashboard?page=bantuan-berbasis-wilayah"
                            data-toggle="tooltip"
                            data-placement="right"
                            title=""
                            data-original-title="Bantuan Berbasis Wilayah">
                            <i class="feather icon-circle"></i>
                            <span class="menu-item">Bantuan Berbasis Wilayah</span>
                        </a>
                    </li>
                    <li id="bantuan-berbasis-nik">
                        <a
                            href="dashboard?page=bantuan-berbasis-nik"
                            data-toggle="tooltip"
                            data-placement="right"
                            title=""
                            data-original-title="Bantuan Berbasis NIK">
                            <i class="feather icon-circle"></i>
                            <span class="menu-item">Bantuan Berbasis NIK</span>
                        </a>
                    </li>
                    <li id="data-dtks">
                        <a
                            href="dashboard?page=data-dtks"
                            data-toggle="tooltip"
                            data-placement="right"
                            title=""
                            data-original-title="Data DTKS">
                            <i class="feather icon-circle"></i>
                            <span class="menu-item">Data DTKS</span>
                        </a>
                    </li>
                    <li id="periode-dtks">
                        <a
                            href="dashboard?page=periode-dtks"
                            data-toggle="tooltip"
                            data-placement="right"
                            title=""
                            data-original-title="Periode DTKS">
                            <i class="feather icon-circle"></i>
                            <span class="menu-item">Periode DTKS</span>
                        </a>
                    </li>
                </ul>
            </li>
        </ul>
    </div>