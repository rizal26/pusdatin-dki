
    <!-- BEGIN: Theme JS-->
    <script src="app-assets/js/core/app-menu.js"></script>
    <script src="app-assets/js/core/app.js"></script>
    <script src="app-assets/js/scripts/components.js"></script>
    <!-- END: Theme JS-->

    <!-- BEGIN: Page Vendor JS-->
    <script src="app-assets/vendors/js/tables/datatable/datatables.min.js"></script>
    <script src="app-assets/vendors/js/tables/datatable/datatables.bootstrap4.min.js"></script>

    <!-- END: Page Vendor JS-->
    <script>
        $(function () {
            var getUrlParameter = function getUrlParameter(sParam) {
                var sPageURL = window.location.search.substring(1),
                    sURLVariables = sPageURL.split('&'),
                    sParameterName,
                    i;

                for (i = 0; i < sURLVariables.length; i++) {
                    sParameterName = sURLVariables[i].split('=');

                    if (sParameterName[0] === sParam) {
                        return typeof sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
                    }
                }
                return false;
            };

            // console.log(getUrlParameter('page'));
            if (getUrlParameter('page') == 'dashboard' || getUrlParameter('page') == '') {
                $('#dashboard').addClass('active');
                $('#breadcrumb-custom').html('<li class="breadcrumb-item active text-bold-700" aria-current="page">Dashboard</li>');
            } else if (
                getUrlParameter('page') == 'pemeringkatan' ) {
                    $('#proxy').addClass('sidebar-group-active');
                    $('#pemeringkatan').addClass('active');
                    html = '<li class="breadcrumb-item active text-bold-700" aria-current="page">Pemeringkatan</li>';
                    $('#breadcrumb-custom').html(html);
                } else if (
                getUrlParameter('page') == 'penyebaran-tingkat-kemiskinan' ) {
                    $('#proxy').addClass('sidebar-group-active');
                    $('#penyebaran-tingkat-kemiskinan').addClass('active');
                    html = '<li class="breadcrumb-item active text-bold-700" aria-current="page">Penyebaran Tingkat Kemiskinan</li>';
                    $('#breadcrumb-custom').html(html);
                } else if (
                getUrlParameter('page') == 'rekomendasi-penerima-bantuan' ) {
                    $('#proxy').addClass('sidebar-group-active');
                    $('#rekomendasi-penerima-bantuan').addClass('active');
                    html = '<li class="breadcrumb-item active text-bold-700" aria-current="page">Rekomendasi Penerima Bantuan</li>';
                    $('#breadcrumb-custom').html(html);
                } else if (
                getUrlParameter('page') == 'bantuan-berbasis-wilayah' ) {
                    $('#proxy').addClass('sidebar-group-active');
                    $('#bantuan-berbasis-wilayah').addClass('active');
                    html = '<li class="breadcrumb-item active text-bold-700" aria-current="page">Bantuan Berbasis Wilayah</li>';
                    $('#breadcrumb-custom').html(html);
                } else if (
                getUrlParameter('page') == 'bantuan-berbasis-nik' ) {
                    $('#proxy').addClass('sidebar-group-active');
                    $('#bantuan-berbasis-nik').addClass('active');
                    html = '<li class="breadcrumb-item active text-bold-700" aria-current="page">Bantuan Berbasis NIK</li>';
                    $('#breadcrumb-custom').html(html);
                } else if (
                getUrlParameter('page') == 'data-dtks' ) {
                    $('#proxy').addClass('sidebar-group-active');
                    $('#data-dtks').addClass('active');
                    html = '<li class="breadcrumb-item active text-bold-700" aria-current="page">Data DTKS</li>';
                    $('#breadcrumb-custom').html(html);
                } else if (
                getUrlParameter('page') == 'periode-dtks' ) {
                    $('#proxy').addClass('sidebar-group-active');
                    $('#periode-dtks').addClass('active');
                    html = '<li class="breadcrumb-item active text-bold-700" aria-current="page">Periode DTKS</li>';
                    $('#breadcrumb-custom').html(html);
                } 
        });
    </script>