<?php
$page = (isset($_GET['page']))? $_GET['page'] : '';

switch($page){
  case 'dashboard': 
  include "content/dashboard.php"; 
  break;

  case 'pemeringkatan': 
  include "content/pemeringkatan.php"; 
  break;

  case 'penyebaran-tingkat-kemiskinan': 
  include "content/penyebaran-tingkat-kemiskinan.php"; 
  break;

  case 'rekomendasi-penerima-bantuan': 
  include "content/rekomendasi-penerima-bantuan.php"; 
  break;

  case 'bantuan-berbasis-wilayah': 
  include "content/bantuan-berbasis-wilayah.php"; 
  break;

  case 'bantuan-berbasis-nik': 
  include "content/bantuan-berbasis-nik.php"; 
  break;

  case 'data-dtks': 
  include "content/data-dtks.php"; 
  break;

  case 'periode-dtks': 
  include "content/periode-dtks.php"; 
  break;
  
  default: 
  include "content/dashboard.php";
}
?>