						<style>
							.card-content-custom { margin-bottom: 25px; }
						</style>
						
						<div class="card bg-authentication rounded-0 mb-0">
                            <div class="row m-0">
                                <div class="col-lg-6 d-lg-block d-none text-center align-self-center px-1 py-0">
                                    <img src="../app-assets/images/pages/login.png" alt="branding logo">
                                </div>
                                <div class="col-lg-6 col-12 p-0">
                                    <div class="card rounded-0 mb-0 px-2">
                                        <div class="card-header pb-1">
                                            <div class="card-title">
												<div class="text-center">
													<img src="../assets/images/pusdatin-dki-logo.png" alt="" width="90" srcset="">
												</div>
                                                <h4 class="mb-0 text-center">SISTEM INFORMASI PEMERINGKATAN PENDUDUK DKI JAKARTA</h4>
                                            </div>
                                        </div>
                                        <p class="px-2 text-center">Pusat Data Informasi Jaminan Sosial<br>Dinas Sosial DKI Jakarta</p>
                                        <div class="card-content card-content-custom">
                                            <div class="card-body pt-1">
                                                <form action="index.html">
                                                    <fieldset class="form-label-group form-group position-relative has-icon-left">
                                                        <input type="text" class="form-control" id="user-name" placeholder="E-mail" required>
                                                        <div class="form-control-position">
                                                            <i class="feather icon-user"></i>
                                                        </div>
                                                        <label for="user-name">E-mail</label>
                                                    </fieldset>

                                                    <fieldset class="form-label-group position-relative has-icon-left">
                                                        <input type="password" class="form-control" id="user-password" placeholder="Password" required>
                                                        <div class="form-control-position">
                                                            <a href="javascript:void(0)" onclick="visible()"><i class="feather icon-eye"></i></a>
                                                        </div>
                                                        <label for="user-password">Password</label>
                                                    </fieldset>
                                                    <div class="form-group d-flex justify-content-between align-items-center">
                                                        <div class="text-left">
                                                            <fieldset class="checkbox">
                                                                <div class="vs-checkbox-con vs-checkbox-primary">
                                                                    <input type="checkbox">
                                                                    <span class="vs-checkbox">
                                                                        <span class="vs-checkbox--check">
                                                                            <i class="vs-icon feather icon-check"></i>
                                                                        </span>
                                                                    </span>
                                                                    <span class="">Remember me</span>
                                                                </div>
                                                            </fieldset>
                                                        </div>
                                                    </div>
                                                    <button type="button" onclick="dashboard()" class="btn btn-primary btn-inline">Login</button>
                                                    <button type="button" class="btn btn-success float-right btn-inline">Pusdatin</button>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>